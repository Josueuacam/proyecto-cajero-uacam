
package ORLANDOYJOSUE;

public class Portal_UAC {

    public static void main(String[] args) {
    int tiempoAtencionTotal;
        Nodo estudianteTiempoMenor;
        Nodo estudianteTiempoMayor;
        int desercion;
        
        int N_Estud;                                                        //Numero de estudiantes Ramdom
        N_Estud = 10 + (int) (Math.random() * 30);                           //Metodo ramdom para obtener el tamaño de la cola
        Cola c = new Cola();
        
        System.out.println("Numero de Estudiantes: " + N_Estud--);
        for (int i = 0; i <= N_Estud; i++) {
            c.insertarNodo();
        }
        c.desplegarCola();
        c.validarCola();
        
        tiempoAtencionTotal = c.getTiempoTotal();
        
        estudianteTiempoMayor = c.getEstudianteMayorTiempo();
        estudianteTiempoMenor = c.getEstudianteMenorTiempo();
        desercion = c.getDesercion();
        
        System.out.println("El tiempo de atencion de toda la cola es de " + tiempoAtencionTotal + " segundos");
        System.out.println("El alumno que espero mas es " + estudianteTiempoMayor.nombre);
        System.out.println("El alumno que espero menos es " + estudianteTiempoMenor.nombre);
        System.out.println("El porcentaje de desercion es " + desercion + "%");

    }

}

class Nodo {

    int tiempoEspera;
    String nombre;
    Nodo siguiente;

}

class Cola {

    Nodo primero;
    Nodo ultimo;
    
    int desertados = 0;
    int alumnTotales = 0;

    public Cola() {
        primero = null;
        ultimo = null;

    }
    
    
    public void insertarNodo() {
        Nodo nuevo = new Nodo();
        int tiempo = 1 + (int) (Math.random() * 70 + 1);                //dando un valor al nodo que puede ser entre 1 y 15
        nuevo.tiempoEspera = tiempo;
        alumnTotales++;

        if (primero == null) {
            primero = nuevo;
            primero.siguiente = null;
            ultimo = nuevo;
        } else {
            ultimo.siguiente = nuevo;
            nuevo.siguiente = null;
            ultimo = nuevo;
        }
    }

    public void desplegarCola() {
        Nodo actual = new Nodo();
        actual = primero;
        int Estudiante = 1;
        if (primero != null) {
            while (actual != null) {
                actual.nombre = "Estudiante " + Estudiante++;
                System.out.println(actual.nombre + "   tiempo: " + "[" + actual.tiempoEspera + " seg]");
                actual = actual.siguiente;
            }
        } else {
            System.out.println("\n la cola esta vacia");
        }
    }

    public void validarCola() {
        Nodo actual = new Nodo();
        Nodo anterior = new Nodo();
        actual = primero;
        
        if (primero != null) {
            while (actual != null) {
                
                if (actual.tiempoEspera <= 50) {
                    System.out.println(actual.nombre + " ha sido Atendido exitosamente");
                    
                } else if (actual.tiempoEspera > 50) {
                    System.out.println(actual.nombre + " a tardado mas de 50 segundo por ende se saco a el ultimo de la cola");
                    ultimo = anterior;
                    anterior.siguiente = null;
                    desertados++;

                } else {
                    System.out.println("No entro ningun dato");

                }
                actual = actual.siguiente;
            }
        }
    }

    
    public int getTiempoTotal(){
        Nodo actual = primero;
        int tiempoAcum = 0;
        if (primero != null){
            while (actual != null) {
                tiempoAcum += actual.tiempoEspera;
                actual = actual.siguiente;
            }
        }
        return tiempoAcum;
    }
    
    public Nodo getEstudianteMenorTiempo(){
        Nodo actual = primero;
        Nodo menor = new Nodo();
        int tiempoMenor = Integer.MAX_VALUE;
        if (primero != null){
            while (actual != null) {
                //System.out.println(actual.tiempoEspera < tiempoMenor);
                if (actual.tiempoEspera < tiempoMenor){
                    menor = actual;
                    tiempoMenor = menor.tiempoEspera;
                }
                actual = actual.siguiente;
            }
        }
        return menor;
    }
    
    public Nodo getEstudianteMayorTiempo(){
        Nodo actual = primero;
        Nodo mayor = new Nodo();
        int tiempoMayor = Integer.MIN_VALUE;
        if (primero != null){
            while (actual != null) {
                if (actual.tiempoEspera > tiempoMayor){
                    mayor = actual;
                    tiempoMayor = mayor.tiempoEspera;
                }
                actual = actual.siguiente;
            }
        }
        return mayor;
    }
    
    public int getDesercion(){
        //return (alumnTotales / desertados) / 100;
        return (desertados * 100) / alumnTotales;
    }
    
}
